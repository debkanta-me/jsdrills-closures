function cacheFunction(cb) {
    let cache = {}

    return function invoke(...param) {
        if(cache[JSON.stringify(param)]){
            console.log("\nExisting cache value returned!");
            return cache[JSON.stringify(param)];
        }
        cache[JSON.stringify(param)] = cb(...param);
        return cb(...param);
    }
}

module.exports = cacheFunction;