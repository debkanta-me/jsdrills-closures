function limitFunctionCallCount(cb, n) {
    let callCount = 0;
    return function invoke(){
        if(callCount<n){
            callCount+=1;
            cb();
        }
    }
}

module.exports = limitFunctionCallCount;
