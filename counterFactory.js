function counterFactory(){
  let counter = 0;
  console.log(`Initial value of counter variable: ${counter}`);
  return {
    increment(){
      return counter+=1;
    },
    decrement(){
      return counter-=1;
    }
  }
}

module.exports = counterFactory;
