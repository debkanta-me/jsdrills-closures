const cacheFunction = require("../cacheFunction");

const invoke = cacheFunction((a, b) => `The division result of ${a} / ${b} is equal to: ${a/b}`);

console.log(invoke(0, 5));
console.log(invoke(5, 0));
console.log(invoke(5, 0));
console.log(invoke(0, 5));
