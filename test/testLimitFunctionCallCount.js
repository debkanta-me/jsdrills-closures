const limitFunctionCallCount = require("../limitFunctionCallCount");

const invoke = limitFunctionCallCount(() => console.log(`Function got invoked!`), 1);

invoke();
invoke();
invoke();
invoke();
invoke();