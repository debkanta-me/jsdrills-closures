const counterFactory = require("../counterFactory");

const {increment, decrement} = counterFactory(10);

console.log(increment());
console.log(increment());
console.log(decrement());
console.log(increment());